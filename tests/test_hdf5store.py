from stuf.six import unittest
from tests.test_store import Store


class TestHDF5Store(Store, unittest.TestCase):

    initstring = 'hdf5://test.hdf5/test'

    def setUp(self):
        from shove import Shove
        self.store = Shove()

    def tearDown(self):
        import os
        self.store.close()
        try:
            os.remove('test.hdf5')
        except OSError:
            pass